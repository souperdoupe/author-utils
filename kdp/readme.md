# KDP for software nerds

Caution: This guide is dry and nerd-friendly. It is a compilation of notes I took in order to publish my first novel, Space Lord Omega: The Limits of Memory. If you're a developer, or someone willing to put some elbow grease into your publishing methods, read and enjoy!

This guide explains some useful, under-the-hood tricks for uploading a home-grown HTML file to KDP. By the end of this, you should have an understanding to make your own Kindle-ready ebook in HTML format.

A lot of self-publishing authors use publishing software of their own choosing. This is *not* intended to replace your favorite publishing software (unless you want to).

These notes are intended to help you understand the way that KDP handles uploads. It also reveals how far you can stretch simple HTML and CSS to make a professional document. Finally, it explains some free, open-source tools intended for publishing professional-looking documents.

# The End Product

We're "beginning with the end in mind." The end product is an HTML file that you can upload straight to KDP.

*For the most part,* this will be a standard HTML file. So, anything you can do with HTML, you can do with your ebook.

With that said, there are a few important need-to-knows about this HTML file:

-	[Character encoding](#character-encoding)

-	[KDP HTML tags](#kdp-html-tags)

-	[The limits of CSS](#the-limits-of-css)

-	[The upload](#the-upload)

## Character encoding

Kindle ebooks use `ISO-8859-1` encoding. By contrast, *most modern browsers* use `UTF-8` encoding. (This will present an annoying challenge in a later section.)

## KDP HTML tags

Kindle offers [their own set of custom html tags](https://kdp.amazon.com/en_US/help/topic/GG5R7N649LECKP7U).  They can be useful for things like page breaks. This way, you can programmatically decide where one chapter ends, and the next one begins.

## The limits of CSS

You can use *most* CSS features, with some exceptions:

-	Remote fonts. These cannot be pulled from external sources. Also, they cannot be embedded through something like base64 encoding.

## The upload

There are two ways you can upload the entire final product:

Method 1: Create an HTML file that contains all the content, like styles and images. Note that images must be embedded in base64. (Pandoc's self-contained and standalone options embed base64 images automatically.)

Method 2: Create a zip file with your HTML and CSS documents. Use relative links in the HTML.

# Your Home-Grown Publishing Kit

This section explains some workarounds for drafting, exporting,and viewing your ebook. Remember Flick, from *A Bug's Life*? The ant with the ludicrous-sounding inventions? This is a Flick-like way to publish.

You can use any, none, or all of these. For a low-level example of "bringing it all together," see the folder here called `mkebook`. It contains a python script which automates a lot of the things I explain below. It's free to view, download, use, or modify.

## Create your draft

I create my drafts using markdown. It's a simple, plaintext markup language which can be exported into professional-looking documents (PDF, HTML, DOCX, etc.)

This approach offers some serious advantages:

-	You can edit it on basically any editor. This opens your writing up to pretty much any situation.

-	It's future-proof. You know how Word will update, and ruin the formatting of your fifteen-year-old document? Markdown avoids that altogether.

-	It's exportable. There are plenty of online or app-based markdown editors and converters.

-	You can integrate HTML tags and LaTeX commands.

That's a lot of hype. By now, you might wonder what a "markdown file" looks like. For reference, this guide --- the one you're reading right now --- was written in markdown. (See `readme.md` to view what it looks like *before* conversion.)

And that's it. Just follow the conventions, and you'll have a functional, easy-to-edit, exportable document, ready for publishing.

## Convert your draft

Once the markdown file is created, I use a utility called pandoc to export it.  Pandoc has its own advantages:

-	Cross-platform. It is available on Windows, Mac, and various \*nixes.

-	Tons of output formats. See the [entire list here](https://pandoc.org/).

-	It has bindings for Python.

-	***You can generate a self-contained HTML file***. This is helpful to avoid multiple documents: HTML, CSS, images, etc. (This is what I did.)

For most authors, the only real drawback is that pandoc is meant for the command line. So, you'll have to research how to 

## Preview your draft

This cannot be stressed enough: *preview your upload in KDP before submitting.* This section is *not* a replacement for such a necessary step. Be skeptical about applications that claim to do "everything."

With that said, you can get a general idea of what your document will look like using your own browser.

1.	Open your HTML file in a *text or HTML editor* (like notepad --- yeah, *that* notepad).

2.	Find the "meta" tag which contains the text, utf-8.

3.	Replace the utf-8 with the text, ISO-8859-1.

4.	Save and close the editor.

5.	Open the HTML file in a *browser*.

This will give you some ideas about the encoding and layout. Note that your fonts may look different once you upload it to KDP.

# Sources

[HTML/Text guidelines](https://kdp.amazon.com/en_US/help/topic/GH4DRT75GWWAGBTU)

[KDP character encoding](https://kdp.amazon.com/en_US/help/topic/G200652310)

[Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[Pandoc output formats](https://pandoc.org/)

[Special HTML tags for KDP ebooks](https://kdp.amazon.com/en_US/help/topic/GG5R7N649LECKP7U)
