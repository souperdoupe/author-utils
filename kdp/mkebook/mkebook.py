#!/usr/bin/python3
import re
import os
import pypandoc


# Generate a final HTML document.
class MakeHtml:
	
	def __init__(self):
		# Pass these as options/arguments later.
		self.name = "ebook_name"
		self.order_file = self.name + "_order.txt"
		self.ext = "html"
		# --v-- These Could be from a config file later.
		# Make a temporary markdown document.
		self.tmp = "." + self.name + "_tmp.md"
		self.tmp_html = "." + self.name + "_tmp.html"
		# Desired output file.
		self.output = self.name + ".html"
		# Page breaker for html document.
		self.page_breaker = "\n<mbp:pagebreak />\n\n"
		# Pandoc arguments.
		self.pandoc_args = [
			'--self-contained',
			'--standalone',
		]
	
	# Modify characters that conflict with ISO-8859-1.
	def html_chars(self, html_file):
		# Replacement strings after HTML is generated.
		re_dict = {
			'utf-8':'iso-8859-1',
			'<p> </p>' : '',
			'---' : '&mdash;',
			'’' : '\'',
			' ' : '&nbsp;',
			'“' : '&quot;',
			'”' : '&quot;',
			'++BREAK++' : '*&emsp;*',
			'++COPYRIGHT++' : '&copy;'
		}
		with open(self.output, 'a') as outfile:
			self.regex_to_file(html_file, re_dict, outfile)
		return 0

	# Convert some text bindings to html tags.
	def tex_to_html(self):
		tmp_file = open(self.tmp, 'a')
		order_file = open(self.order_file, 'r')
		regex = {
			"\newpage" : "<mbp:pagebreak />",
			"\begin{centering}" : "<center>",
			"\end{centering}" : "</center>",
			"\begin{center}" : "<center>",
			"\end{center}" : "</center>",
			"\setlength.*" : "",
			"\textcopyright" : "++COPYRIGHT++"
		}
		# Loop through each chapter/section, in order.
		for chapter in order_file:
			# Remove unncessary characters.
			chapter = re.sub("[\n\t\s]*", '', chapter)
			self.regex_to_file(chapter.strip('\t').strip('\n'), 
								regex, tmp_file)
			tmp_file.write(self.page_breaker)
			print("Appended " + chapter + "to tempfile.")
		order_file.close()
		tmp_file.close()
		return 0
	
	# Write regex-filtered lines to a file.
	def regex_to_file(self, infile, regex_dict, outfile):
		# Line by line, make replacements.
		with open(infile, 'r') as f:
			for line in f:
				# Loop through keys/values.
				for key in regex_dict:
					# Compile the pattern object.
					line = re.sub(
						re.escape(key),
						regex_dict[key],
						line)
				outfile.write(line)
		f.close()
		return 0

	# The meat and potatoes of this fucking script.
	def to_html(self):
		# Generate a new tempfile.
		try:
			os.remove(self.tmp)
		except:
			pass
		# Compile everything into a single markdown file. Creates
		# an updated tmpfile.
		self.tex_to_html()
		# Convert the tmpfile to the desied output.
		with open(self.tmp_html, 'a') as conv:
			conv.write(pypandoc.convert_file(
				self.tmp, 'html5', format='md',
				extra_args=self.pandoc_args))
		os.remove(self.tmp)
		# Clean up the character encoding.
		self.html_chars(self.tmp_html)
		os.remove(self.tmp_html)
		return 0
		

if __name__ == "__main__":
	h = MakeHtml()
	h.to_html()
	
