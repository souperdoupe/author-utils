# mkebook.py

I'm half asleep but this was my solution for getting an eBook uploaded.

## Purpose

Generate a KDP-friendly HTML file.

tl;dr, Kindle uses ISO-8859-1 encoding. This presents a huge pain in
the neck for anyone used to UTF-8 --- so, like, everyone. KDP should 
be shut down for this but here we are.

The workaround is to take all the LaTeX bindings, and either

-	Remove them

-	Convert them to ISO-8859-1-compliant identities

-	Convert them to HTML tags

This script does all of that. The final product should be readable
when you upload it to KDP.

## You're still reading? Ok, well...

Pandoc automatically converts a bunch of characters. These character
types are UTF-8 only. (Pandoc's developer is diametrically opposed to 
anything but UTF-8. That makes this problem, my problem.)

Because of the UTF-8 drama, you have to change characters in two phases.
That is, you change the "big" latex bindings first. Then, you change 
the UTF-8 characters to HTML identities.

## Usage

You will need:

-	Your ebook, in markdown, separated into chapters

-	Existing LaTeX bindings in said markdown files

-	A list of all the file names in order

-	To change the variable names in the script to match your own

Do this, run the script, and it should produce your html file.

## Future releases

I'll probably change the entire thing.

No, really:

-	Pass filenames as arguments

-	Have some kind of optional INI file for something I already forgot

-	Change the entire thing hahaha jk (...unless?)

## So if you notice

there's a bunch of class variables. Like half of those should be in the
main function.

The main function.

That one at the end.

Or whatever.

Anyway some of them really shouldn't be class functions.
