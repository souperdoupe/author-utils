# spml

MarkUp language for screenplay formatting.

This tool takes simple markup and converts it to a LaTeX-friendly language.  
This project was inspired by markdown.

This project was started about thirty minutes ago.  It is currently a WIP.

# Requirements

-	Knowledge of regular expressions
-	Knowledge of screenplay writing
-	LaTeX with `\documentclass{screenplay}` in the preamble

# Using smpl

## Example

An example of what you would type would look like this:

```
INT. A SHADY APARTMENT

Samwise enters a horrible studio apartment.  Chicken wing bones litter
old plates.  Mold is visibly growing on the sink.

;SAMWISE
(Concerned)
Oh, boy, Mr. Frodo, Bilbo is gonna kill us now!;;

;FRODO
(Unconcerned)
It doesn't matter. (Gazes at ring) We have far
greater concerns than chicken wings...;;

Elrond, asleep on the couch, slowly wakes up.  He is visibly drunk and
[...]
```

After conversion, it will become a PDF file that looks something like this:

![](./smpl_example.PNG)

# Advanced

If you're interested in hacking the backend yourself, give this
section a read.

## Backend

The table below explains the regex commands used for conversion.

Convention | spml syntax | Regex finds | Regex replace (to LaTeX)
---|---|---|---
**DIALOGUE** |||
Character name | ;CHARACTER | ;(.*?)$ | \\begin{dialogue}{\1}
End of dialogue | ;; | ;;$ | \end{dialogue}
Parentheticals | (Parenthetical comment) | \((.*?)\) | \\paren{\1}
**SCENE/SETTING** |||
Internal | INT. | ^INT. (.*?) | \\intslug{\1}
External | EXT. | ^EXT. (.*?) | \\extslug{\1}

# Further notes

The following is a simple template for a screenplay preamble.

```
\documentclass{screenplay}[date/of/revision]
\geometry{a4paper, margin=1in}
\title{Title Of Your Screenplay}
\author{Your pen name}
\address{My Address}
```

The `\coverpage` command can be used in the document body to create a simple
cover page.
