/* 
spml
====
* Screenplay markup language 
* Developed by souperdoupe 

References
----------
* https://alvinalexander.com/blog/post/java/how-open-read-file-java-string-array-list
* https://www.caveofprogramming.com/java/java-file-reading-and-writing-files-in-java.html

Priority goals
--------------
* Perform the regex
* Save output as a tex file
* Convert the tex file to a PDF document

Future ideas
------------
* Use pandoc to make html version
* Maybe include a header string for spml docs
* See if Java can do LaTeX output with a class or something.

*/

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.FileWriter;
import java.time.LocalDate;


public class spml {

	public static void main(String[] args) 
	{ 
		// Example of how you'd get the script data.
		String input_file = "/tmp/test.txt";
		String texfile = "/tmp/test.tex";
		// Store the script as a list (potentially massive).
		List<String> script = getScript(input_file);
		// Apply tex bindings and write the script to texfile path.
		int stat = writeToFile(script, texfile);
		if (stat == 0)
		{
			System.out.println("Done.");
		}
		else { System.out.println("Errors occurred."); }
		// Produce a PDF document.
		makePdf(texfile);
	}

	// Open the actual script.  Inut filename, return its contents.
	private static List<String> getScript(String filename)
	{
		List<String> script = new ArrayList<String>();
        // This will reference one line at a time
        String line = null;
        try
        {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(filename);
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) {
				line = convertMarkup(line);
				script.add(line);
            }
            // Always close files.
            bufferedReader.close();
            // Return the script.
            return script;
        }
        // If file isn't found.
        catch(FileNotFoundException ex)
        {
            System.out.println(
                "Unable to open file '" + 
                filename + "'");
            return null;              
        }
        // General errors.
        catch(IOException ex)
        {
            System.out.println(
                "Error reading file '" 
                + filename + "'");                  
            // Or we could just do this: 
            // ex.printStackTrace();
			return null;
        }
	}
	
	// Perform regex substitutions; return the line.
	private static String convertMarkup(String line)
	{
		// Return a line in tex notation.
		String line_tex = new String();
		// Set patterns to be matched. Surely, there's a better way to do this.
		Matcher begin_dialogue = Pattern.compile("^@(.*?)$").matcher(line);
		Matcher end_dialogue = Pattern.compile(";;$").matcher(line);
		Matcher parenthetical = Pattern.compile("\\(").matcher(line);
		Matcher internal = Pattern.compile("^INT. (.*?)$").matcher(line);
		Matcher external = Pattern.compile("^EXT. (.*?)$").matcher(line);
		// Begin dialogue (character's name).
		if (begin_dialogue.find())
		{
			line_tex = "\\begin{dialogue}{" + begin_dialogue.group(1) + "}";
		}
		// End dialogue.
		else if(end_dialogue.find())
		{
			line_tex = "\\end{dialogue}";
		}
		// Replace all parentheticals in the line.
		else if(parenthetical.find())
		{
			// Replace parentheticals.
			line_tex = line.replaceAll("\\(", "\\\\paren{");
			line_tex = line_tex.replaceAll("\\)", "}");
		}
		// Setting: internal.
		else if(internal.find())
		{
			line_tex = "\\intslug{" + internal.group(1) + "}";
		}
		// Setting: External.
		else if(external.find())
		{
			line_tex = "\\extslug{" + external.group(1) + "}";
		}
		// Default to the input string..
		else { line_tex = line; }
		// Return the line.
		return line_tex;
	}
	
	// Write to file.
	private static int writeToFile(List<String> script, String texfile)
	{
		try
		{
			FileWriter fw = new FileWriter(texfile);
			// Create a preamble.  Later: check for custom...
			LocalDate date = java.time.LocalDate.now();
			//String date = "date";
			fw.write("\\documentclass{screenplay}[" + date + "]\n" +
				"\\begin{document}\n");
			// Append the script.
			int script_length = script.size();
			for(String line:script)
			{
				System.out.println(line);
				fw.write(line + "\n");
			}
			fw.write("\\end{document}");
			fw.close();
			return 0;
		}
		catch(IOException e) 
		{
			System.out.println(e);
			return 1;
		}
	}
	
	// Convert tex file to PDF.
	// Caveat: requires pdflatex w/screenplay class to be installed.
	private static void makePdf(String input_tex)
	{
		try
		{
			Runtime.getRuntime().exec("pdflatex " + input_tex);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
